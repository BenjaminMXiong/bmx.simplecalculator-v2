// Ben Xiong Simple Calculator
#include <iostream>
#include <conio.h>
using namespace std;
//This helps create the equation so the user can type it in the console
float add(float, char, float);
float sub(float, char, float);
float mult(float, char, float);
float divi(float, char, float);


float add(float a, char o, float b)
{
	if (o == '+')
		return (a + b);
}
float sub(float a, char o, float b)
{
	if (o == '-')
		return (a - b);
}
float mult(float a, char o, float b)
{
	if (o == '*')
		return (a * b);
}
float divi(float a, char o, float b)
{
	if (o == '/' && b != 0)
		return (a / b);
}


int main()
{
	int a, b;
	char o;
	char end = 'y';
	while (end != 'n')
	{
		cout << "Enter a two factor equation with positive numbers: " << endl;
		cin >> a >> o >> b;



		if (o == '+')
		{
			cout << "Answer: " << add(a, o, b) << endl;
		}

		else if (o == '-')
		{
			cout << "Answer: " << sub(a, o, b) << endl;

		}

		else if (o == '*')
		{
			cout << "Answer: " << mult(a, o, b) << endl;
		}

		else if (o == '/' && b == 0)
		{
			cout << "Division by zero is not possible." << endl;
		}
		else
		{
			cout << "Answer: " << divi(a, o, b) << endl;

		}


		cout << "Will you like to continue? Enter y or n: ";
		cin >> end; 
	}

	return 0;
	_getch();

}



